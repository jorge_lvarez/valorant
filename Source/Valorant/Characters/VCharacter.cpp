// Fill out your copyright notice in the Description page of Project Settings.


#include "VCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "Engine/Engine.h"
#include "../Components/Characters/Common/VCharacterMovementComponent.h"
#include "../Components/Characters/Common/WeaponManagerComponent.h"
#include "Blueprint/UserWidget.h"
#include "../HUD/VHUD.h"
#include "Kismet/GameplayStatics.h"

//Sets default values
AVCharacter::AVCharacter()
{
	//Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	//Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	//Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	Mesh1P->SetOnlyOwnerSee(false);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;

	//Create the movement component for the character
	MovementComponent = CreateDefaultSubobject<UVCharacterMovementComponent>(TEXT("MovementComponent"));
	//Create the weapon manager component for the character
	WeaponManager = CreateDefaultSubobject<UWeaponManagerComponent>(TEXT("WeaponManagerComponent"));
}

USkeletalMeshComponent * AVCharacter::GetArmsMesh()
{
	return Mesh1P;
}

void AVCharacter::BuyWeapon(TSubclassOf<AVWeapon> WeaponType, int WeaponSlot)
{
	WeaponManager->SpawnAndAttachWeapon(WeaponType, WeaponSlot, true);
}

// Called when the game starts or when spawned
void AVCharacter::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AVCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AVCharacter::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AVCharacter::Fire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AVCharacter::StopFiring);

	//Movement state change events
	PlayerInputComponent->BindAction("Walk", IE_Pressed, this->MovementComponent, &UVCharacterMovementComponent::StartWalking);
	PlayerInputComponent->BindAction("Walk", IE_Released, this->MovementComponent, &UVCharacterMovementComponent::StopWalking);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this->MovementComponent, &UVCharacterMovementComponent::StartCrouching);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this->MovementComponent, &UVCharacterMovementComponent::StopCrouching);

	//Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this->MovementComponent, &UVCharacterMovementComponent::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this->MovementComponent, &UVCharacterMovementComponent::MoveRight);

	//Camera Rotation
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAxis("ChangeWeapon", this->WeaponManager, &UWeaponManagerComponent::ScrollWeaponSlot);
	PlayerInputComponent->BindAction("ChangeToKnife", IE_Pressed, this->WeaponManager, &UWeaponManagerComponent::ChangeWeaponSlotToKnife);
	PlayerInputComponent->BindAction("ChangeToPistol", IE_Pressed, this->WeaponManager, &UWeaponManagerComponent::ChangeWeaponSlotToPistol);
	PlayerInputComponent->BindAction("ChangeToRifle", IE_Pressed, this->WeaponManager, &UWeaponManagerComponent::ChangeWeaponSlotToRifle);

	PlayerInputComponent->BindAction("ToggleShopWindow", IE_Pressed, this, &AVCharacter::ToggleShowWindow);
}

void AVCharacter::Fire()
{
	WeaponManager->GetCurrentWeapon()->Shoot();
}

void AVCharacter::StopFiring()
{
	WeaponManager->GetCurrentWeapon()->ShootButtonReleased();
}

void AVCharacter::ToggleShowWindow()
{
	if (ShopWindowEnabled)
	{
		CloseShopWindow();
		ShopWindowEnabled = false;
	}
	else
	{
		OpenShopWindow();
		ShopWindowEnabled = true;
	}
}

void AVCharacter::OpenShopWindow()
{
	AVHUD * hud = Cast<AVHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());

	hud->OpenShopWindow();

	APlayerController * controller = Cast<APlayerController>(GetController());
	controller->bShowMouseCursor = true;
	controller->bEnableClickEvents = true;
	controller->bEnableMouseOverEvents = true;
}

void AVCharacter::CloseShopWindow()
{
	AVHUD * hud = Cast<AVHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());

	hud->CloseShopWindow();

	APlayerController * controller = Cast<APlayerController>(GetController());
	controller->bShowMouseCursor = false;
	controller->bEnableClickEvents = false;
	controller->bEnableMouseOverEvents = false;
}
