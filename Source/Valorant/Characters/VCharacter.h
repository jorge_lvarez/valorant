// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Weapons/Knife/VKnife.h"
#include "../Weapons/Pistols/VPistol.h"
#include "../Weapons/Rifles/VRifle.h"
#include "../Components/Characters/Common/WeaponManagerComponent.h"
#include "Components/WidgetComponent.h"
#include "VCharacter.generated.h"

UCLASS()
class VALORANT_API AVCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	/** Functions */
	// Sets default values for this character's properties
	AVCharacter();
	
	USkeletalMeshComponent * GetArmsMesh();

	UFUNCTION(BlueprintCallable)
		void BuyWeapon(TSubclassOf<AVWeapon> WeaponType, int WeaponSlot);

	/** Variables */
	// First person camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		class UCameraComponent * FirstPersonCameraComponent;

	// Pawn mesh: 1st person view (arms; seen only by self)
	UPROPERTY(VisibleAnywhere, Category = Mesh)
		class USkeletalMeshComponent * Mesh1P;

	// Component in charge of handling the character movement
	UPROPERTY(VisibleDefaultsOnly, Category = "Components | Movement")
		class UVCharacterMovementComponent * MovementComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components | Combat")
		class UWeaponManagerComponent * WeaponManager;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent * PlayerInputComponent) override;

private:
	/** Functions */
	// Fires weapon
	void Fire();
	void StopFiring();

	void ToggleShowWindow();
	void OpenShopWindow();
	void CloseShopWindow();

	/** Variables */
	bool ShopWindowEnabled = false;
};
