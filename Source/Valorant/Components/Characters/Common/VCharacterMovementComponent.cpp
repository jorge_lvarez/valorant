// Fill out your copyright notice in the Description page of Project Settings.


#include "VCharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "Components/SceneComponent.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/Engine.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values for this component's properties
UVCharacterMovementComponent::UVCharacterMovementComponent()
{
	//Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	//off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//Get character
	character = static_cast<AVCharacter*>(GetOwner());
}


// Called when the game starts
void UVCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	//Set movement comp so that the character can crouch and jump
	CharacterMovComp = static_cast<UCharacterMovementComponent*>(character->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	CharacterMovComp->NavAgentProps.bCanCrouch = true;
	CharacterMovComp->NavAgentProps.bCanJump = true;

	USkeletalMeshComponent * arms = character->GetArmsMesh();

	baseCameraHeight = character->FirstPersonCameraComponent->GetComponentLocation().Z;
	crouchCameraHeight = baseCameraHeight - crouchCameraHeightOffset;
}

void UVCharacterMovementComponent::StartInterpolatedCrouch(float DeltaTime)
{
	currentCrouchValue = FMath::FInterpConstantTo(currentCrouchValue, 1.0f, DeltaTime, 1.0f / timeToCrouch);

	const float newHeight = baseCameraHeight - currentCrouchValue * (baseCameraHeight - crouchCameraHeight);

	// Calculate where our new location will be - Only adjust if we're on the ground
	if (CharacterMovComp->IsMovingOnGround())
	{
		UCameraComponent * characterCamera = character->FirstPersonCameraComponent;

		FVector location = characterCamera->GetComponentLocation();
		location.Z = newHeight;
		characterCamera->SetWorldLocation(location);
	}
}

void UVCharacterMovementComponent::StopInterpolatedCrouch(float DeltaTime)
{
	currentCrouchValue = FMath::FInterpConstantTo(currentCrouchValue, 0.0f, DeltaTime, 1.0f / timeToCrouch);

	const float newHeight = baseCameraHeight - currentCrouchValue * (baseCameraHeight - crouchCameraHeight);

	// Calculate where our new location will be - Only adjust if we're on the ground
	if (CharacterMovComp->IsMovingOnGround())
	{
		UCapsuleComponent * characterCapsule = character->GetCapsuleComponent();
		UCameraComponent * characterCamera = character->FirstPersonCameraComponent;

		FVector location = characterCamera->GetComponentLocation();
		location.Z = newHeight;
		characterCamera->SetWorldLocation(location);
	}
}

// Called every frame
void UVCharacterMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (wantsToCrouch)
	{
		if(!FMath::IsNearlyEqual(currentCrouchValue, 1.0f))
		{
			StartInterpolatedCrouch(DeltaTime);
		}
	}
	else
	{
		if (!FMath::IsNearlyZero(currentCrouchValue))
		{
			StopInterpolatedCrouch(DeltaTime);
		}
	}
}

void UVCharacterMovementComponent::MoveForward(float Value)
{
	float movementSpeed = RunSpeed;

	if (isCrouching)
	{
		movementSpeed = CrouchSpeed;
	}
	else if (isWalking)
	{
		movementSpeed = WalkSpeed;
	}

	//Move the character forward/backwards
	character->AddMovementInput(character->GetCapsuleComponent()->GetForwardVector(), Value * movementSpeed);
}

void UVCharacterMovementComponent::MoveRight(float Value)
{
	float movementSpeed = RunSpeed;

	if (isCrouching)
	{
		movementSpeed = CrouchSpeed;
	}
	else if (isWalking)
	{
		movementSpeed = WalkSpeed;
	}

	//Move the character left/right
	character->AddMovementInput(character->GetCapsuleComponent()->GetRightVector(), Value * movementSpeed);
}

void UVCharacterMovementComponent::StartWalking()
{
	isWalking = true;

	if (!isCrouching)
	{
		character->GetArmsMesh()->SetPlayRate(WalkSpeed);
	}
}

void UVCharacterMovementComponent::StopWalking()
{
	isWalking = false;

	if (!isCrouching)
	{
		character->GetArmsMesh()->SetPlayRate(RunSpeed);
	}	
}

void UVCharacterMovementComponent::StartCrouching()
{
	isCrouching = true;

	character->GetArmsMesh()->SetPlayRate(CrouchSpeed);

	wantsToCrouch = true;
}

void UVCharacterMovementComponent::StopCrouching()
{
	isCrouching = false;

	character->GetArmsMesh()->SetPlayRate(RunSpeed);

	wantsToCrouch = false;
}