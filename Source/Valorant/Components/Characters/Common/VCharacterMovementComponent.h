// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UObject/ObjectMacros.h"
#include "Valorant/Characters/VCharacter.h"
#include "VCharacterMovementComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VALORANT_API UVCharacterMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	/** Functions */
	// Sets default values for this component's properties
	UVCharacterMovementComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Handles moving forward/backward
	void MoveForward(float Value);

	// Handles moving, left and right
	void MoveRight(float Value);

	void StartWalking();
	void StopWalking();
	void StartCrouching();
	void StopCrouching();

	/** Variables */
	UPROPERTY(BlueprintReadOnly)
	bool isWalking = false;
	UPROPERTY(BlueprintReadOnly)
	bool isCrouching = false;
	UPROPERTY(EditAnywhere, Category = "Movement Settings")
	float RunSpeed = 1.0f;
	UPROPERTY(EditAnywhere, Category = "Movement Settings")
	float WalkSpeed = 0.6f;
	UPROPERTY(EditAnywhere, Category = "Movement Settings")
	float CrouchSpeed = 0.2f;
	UPROPERTY(EditAnywhere, Category = "Movement Settings")
	float timeToCrouch = 0.2f;
	UPROPERTY(EditAnywhere, Category = "Movement Settings")
	float crouchCameraHeightOffset = 50.0f;

protected:
	/** Functions */
	// Called when the game starts
	virtual void BeginPlay() override;

	/** Variables */

private:
	/** Functions */
	void StartInterpolatedCrouch(float DeltaTime);
	void StopInterpolatedCrouch(float DeltaTime);

	/** Variables */
	AVCharacter * character = nullptr;
	UCharacterMovementComponent * CharacterMovComp = nullptr;
	float currentCrouchValue = 0.0f;	
	float crouchCameraHeight;
	float baseCameraHeight;
	bool wantsToCrouch = false;
};
//