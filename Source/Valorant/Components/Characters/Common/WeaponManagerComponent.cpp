// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponManagerComponent.h"
#include "Components/ActorComponent.h"
#include "Valorant/Weapons/VWeapon.h"
#include "Valorant/Characters/VCharacter.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

// Sets default values for this component's properties
UWeaponManagerComponent::UWeaponManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	//Spawn knife and attach it to hand socket
	AVWeapon * spawnedKnife = GetWorld()->SpawnActor<AVWeapon>(KnifeType, GetOwner()->GetActorLocation(), FRotator::ZeroRotator);
	spawnedKnife->AttachToComponent(Cast<USceneComponent>(static_cast<AVCharacter*>(GetOwner())->GetArmsMesh()), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "KnifeGripPoint");

	weapons.knife = spawnedKnife;	
}


// Called every frame
void UWeaponManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UWeaponManagerComponent::ScrollWeaponSlot(float Value)
{
	if (Value > 0)
	{
		if (ChangeToWeaponSlot(currentWeaponSlot - 1) == false)
		{
			ChangeToWeaponSlot(currentWeaponSlot - 2);
		}
	}
	else if (Value < 0)
	{
		if (ChangeToWeaponSlot(currentWeaponSlot + 1) == false)
		{
			ChangeToWeaponSlot(currentWeaponSlot + 2);
		}
	}
}

bool UWeaponManagerComponent::ChangeToWeaponSlot(int Index)
{
	if (Index < 0)
	{
		Index = 0;
	}
	else if (Index > 2)
	{
		Index = 2;
	}	

	if (!IsValid(weapons.container[Index]))
	{
		return false;
	}

	weapons.container[currentWeaponSlot]->SetActorHiddenInGame(true);

	currentWeaponSlot = Index;

	weapons.container[currentWeaponSlot]->SetActorHiddenInGame(false);

	return true;
}

void UWeaponManagerComponent::ChangeWeaponSlotToKnife()
{
	ChangeToWeaponSlot(0);
}

void UWeaponManagerComponent::ChangeWeaponSlotToPistol()
{
	ChangeToWeaponSlot(1);
}

void UWeaponManagerComponent::ChangeWeaponSlotToRifle()
{
	ChangeToWeaponSlot(2);
}

void UWeaponManagerComponent::SpawnAndAttachWeapon(TSubclassOf<AVWeapon> weapon, int SlotIndex, bool overrideSlot)
{
	if (weapons.container[SlotIndex] != nullptr)
	{
		if (overrideSlot)
		{
			weapons.container[SlotIndex]->Destroy();
		}
		else
		{
			return;
		}
	}

	//Spawn weapon and attach it to hand socket
	AVWeapon * spawnedWeapon = GetWorld()->SpawnActor<AVWeapon>(weapon, GetOwner()->GetActorLocation(), FRotator::ZeroRotator);
	spawnedWeapon->AttachToComponent(Cast<USceneComponent>(static_cast<AVCharacter*>(GetOwner())->GetArmsMesh()), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "GripPoint");

	weapons.container[SlotIndex] = spawnedWeapon;
	
	if (currentWeaponSlot == 0)	//Just has knife
	{
		currentWeaponSlot = SlotIndex;
	}
	else
	{
		weapons.container[SlotIndex]->SetActorHiddenInGame(true);
	}
}

AVWeapon * UWeaponManagerComponent::GetCurrentWeapon()
{
	return weapons.container[currentWeaponSlot];
}
