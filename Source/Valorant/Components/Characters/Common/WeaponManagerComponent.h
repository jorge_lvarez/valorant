// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Valorant/Weapons/VWeapon.h"
#include "WeaponManagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VALORANT_API UWeaponManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	/** Functions */
	// Sets default values for this component's properties
	UWeaponManagerComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ScrollWeaponSlot(float Value);
	bool ChangeToWeaponSlot(int Index);
	void ChangeWeaponSlotToKnife();
	void ChangeWeaponSlotToPistol();
	void ChangeWeaponSlotToRifle();

	UFUNCTION(BlueprintCallable)
	void SpawnAndAttachWeapon(TSubclassOf<AVWeapon> weapon, int SlotIndex, bool overrideSlot = false);

	AVWeapon * GetCurrentWeapon();

	/** Variables */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapons)
		TSubclassOf<AVWeapon> KnifeType;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapons)
	//	TSubclassOf<AVWeapon> PistolType;
	//
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapons)
	//	TSubclassOf<AVWeapon> RifleType;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapons)
		TArray<TSubclassOf<AVWeapon>> PistolWeaponPool;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapons)
		TArray<TSubclassOf<AVWeapon>> RifleWeaponPool;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:	
	/** Functions */

	/** Variables */
	union
	{
		AVWeapon * container[3] = { nullptr };

		struct
		{
			AVWeapon * knife;
			AVWeapon * pistol;
			AVWeapon * rifle;
		};
	} weapons;

	int currentWeaponSlot = 0;
	int currentPistolIndex = 0;
	int currentRifleIndex = 6;
};
