// Fill out your copyright notice in the Description page of Project Settings.


#include "VHUD.h"

AVHUD::AVHUD()
{

}

void AVHUD::DrawHUD()
{
	Super::DrawHUD();
}

void AVHUD::BeginPlay()
{
	Super::BeginPlay();
}

void AVHUD::OpenShopWindow()
{
	ShopWindowInstance = CreateWidget<UUserWidget>(GetGameInstance(), ShopWindow, "ShopWindowWidget");

	ShopWindowInstance->AddToViewport();
}

void AVHUD::CloseShopWindow()
{
	ShopWindowInstance->RemoveFromParent();
}
