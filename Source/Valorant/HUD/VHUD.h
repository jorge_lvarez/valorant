// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "VHUD.generated.h"

/**
 * 
 */
UCLASS()
class VALORANT_API AVHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	/** Functions */
	AVHUD();

	//Primary draw call for the HUD
	virtual void DrawHUD() override;
	virtual void BeginPlay() override;

	void OpenShopWindow();
	void CloseShopWindow();

	/** Variables */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class UUserWidget> ShopWindow;

	UUserWidget * ShopWindowInstance = nullptr;
};

