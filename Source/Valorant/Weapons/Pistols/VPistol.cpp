// Fill out your copyright notice in the Description page of Project Settings.


#include "VPistol.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/Engine.h"

void AVPistol::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AVPistol::Shoot()
{
	if (!CanShoot())
	{
		return;
	}

	FHitResult hit = ShootRay("PistolTrace");	

	DrawDebugLine(GetWorld(), hit.TraceStart, hit.TraceEnd, FColor::Red, false, 0.2f);

	//Debug line starting from weapon muzzle
	UStaticMeshComponent * muzzle = static_cast<UStaticMeshComponent*>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	DrawDebugLine(GetWorld(), muzzle->GetComponentLocation(), hit.TraceEnd, FColor::Blue, false, 0.2f);

	FString text = FString("RemainingAmmo").Append(FString::SanitizeFloat(RemainingAmmo));
	FString text2 = FString("RemainingClipAmmo").Append(FString::SanitizeFloat(RemainingClipAmmo));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, text);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, text2);

	UsedAmmo();

	if (RemainingClipAmmo == 0)
	{
		//Reload
		Reload();
	}
}

void AVPistol::BeginPlay()
{
	Super::BeginPlay();
}
