// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../VWeapon.h"
#include "VPistol.generated.h"

/**
 * 
 */
UCLASS()
class VALORANT_API AVPistol : public AVWeapon
{
	GENERATED_BODY()
	
public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Shoot() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


};
