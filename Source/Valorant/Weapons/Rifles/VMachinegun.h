// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Rifles/VRifle.h"
#include "VMachinegun.generated.h"

/**
 * 
 */
UCLASS()
class VALORANT_API AVMachinegun : public AVRifle
{
	GENERATED_BODY()
	
};
