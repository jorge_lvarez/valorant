// Fill out your copyright notice in the Description page of Project Settings.


#include "VRifle.h"
#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/Engine.h"

AVRifle::AVRifle()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AVRifle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CanShoot())
	{
		if (IsShooting)
		{
			FireRateCounter += DeltaTime;

			if (FireRateCounter >= FireRate)
			{
				FHitResult hit = ShootRay("RifleTrace");

				DrawDebugLine(GetWorld(), hit.TraceStart, hit.TraceEnd, FColor::Red, false, 0.2f);

				//Debug line starting from weapon muzzle
				UStaticMeshComponent * muzzle = static_cast<UStaticMeshComponent*>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
				DrawDebugLine(GetWorld(), muzzle->GetComponentLocation(), hit.TraceEnd, FColor::Blue, false, 0.2f);

				FireRateCounter = 0.0f;

				UsedAmmo();


				FString text = FString("RemainingAmmo").Append(FString::SanitizeFloat(RemainingAmmo));
				FString text2 = FString("RemainingClipAmmo").Append(FString::SanitizeFloat(RemainingClipAmmo));
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, text);
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, text2);


				if (RemainingClipAmmo == 0)
				{
					//Reload
					Reload();
				}
			}
		}
	}

}

void AVRifle::Shoot()
{
	IsShooting = true;
}

void AVRifle::ShootButtonReleased()
{
	IsShooting = false;
	FireRateCounter = FireRate;	//Reset counter
}

void AVRifle::BeginPlay()
{
	Super::BeginPlay();

	FireRateCounter = FireRate;	//Start being able to shoot
}
