// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Rifles/VRifle.h"
#include "VShotgun.generated.h"

/**
 * 
 */
UCLASS()
class VALORANT_API AVShotgun : public AVRifle
{
	GENERATED_BODY()
	
};
