// Fill out your copyright notice in the Description page of Project Settings.


#include "VWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Engine.h"

// Sets default values
AVWeapon::AVWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
	this->SetRootComponent(Mesh);
}

// Called when the game starts or when spawned
void AVWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	RemainingAmmo = TotalAmmo;
	RemainingClipAmmo = AmmoPerClip;
}

FHitResult AVWeapon::ShootRay(FName TraceName)
{
	APlayerController * controller = GetWorld()->GetFirstPlayerController();

	if (controller == nullptr) //We access the controller, make sure we have one, else we will crash
	{
		return {};
	}

	FVector CamLoc;
	FRotator CamRot;

	controller->GetPlayerViewPoint(CamLoc, CamRot);

	const FVector StartTrace = CamLoc;
	const FVector ShootDir = CamRot.Vector();
	const FVector EndTrace = StartTrace + ShootDir * 5000.0f;

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(TraceName, true, this);

	FHitResult Hit(ForceInit);

	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility /*Change this later*/, TraceParams);

	return Hit;
}

// Called every frame
void AVWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Reloading)
	{
		//FString text = FString("Reload Time  ").Append(FString::SanitizeFloat(ReloadTime));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, text);

		if (ReloadCounter <= ReloadTime)
		{
			ReloadCounter += GetWorld()->GetDeltaSeconds();
		}
		else
		{
			RemainingClipAmmo = AmmoPerClip;
			Reloading = false;
			ReloadCounter = 0.0f;
		}
	}
}

void AVWeapon::Reload()
{
	Reloading = true;
}

bool AVWeapon::CanShoot()
{
	if (RemainingAmmo == 0 || Reloading)	//No Ammo Left or reloading
	{
		return false;
	}

	return true;
}

void AVWeapon::UsedAmmo(unsigned RoundsUsed)
{
	RemainingAmmo -= RoundsUsed;

	RemainingClipAmmo = (RoundsUsed > RemainingClipAmmo) ? 0 : RemainingClipAmmo - RoundsUsed;
}

