// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VWeapon.generated.h"

UCLASS()
class VALORANT_API AVWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVWeapon();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Shoot() {};
	virtual void ShootButtonReleased() {};
	virtual void Reload();
	virtual void AlternativeButtonPressed() {};
	virtual void AlternativeButtonReleased() {};
	virtual bool CanShoot();
	virtual void UsedAmmo(unsigned RoundsUsed = 1);

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	class USkeletalMeshComponent * Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FHitResult ShootRay(FName TraceName);

	UPROPERTY(EditAnywhere, Category = Settings)
	unsigned TotalAmmo;
	UPROPERTY(EditAnywhere, Category = Settings)
	unsigned AmmoPerClip;
	UPROPERTY(EditAnywhere, Category = Settings)
	unsigned Price;
	UPROPERTY(EditAnywhere, Category = Settings)
	float ReloadTime;
	UPROPERTY(EditAnywhere, Category = Settings)
	float Accuracy;
	UPROPERTY(EditAnywhere, Category = Settings)
	float FireRate;	
	UPROPERTY(EditAnywhere, Category = Settings)
	bool AutoFire;
	UPROPERTY(EditAnywhere, Category = Settings)
	TArray<FVector> BulletDispersion;

	unsigned RemainingAmmo;
	unsigned RemainingClipAmmo;
	bool Reloading = false;
	float ReloadCounter = 0.0f;

private:	

};
